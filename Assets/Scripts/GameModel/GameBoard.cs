
using System.Collections.Generic;

public class GameBoard
{
    private Cell[,] cells;

    public readonly int boardSize;

    public GameBoard(int boardSize)
    {
        this.boardSize = boardSize;
        cells = new Cell[boardSize, boardSize];

        FillGameBoard();
    }

    private void FillGameBoard()
    {
        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                cells[i, j] = new Cell();
            }
        }
    }

    public void ResetGameBoard()
    {
        for (int i = 0; i < boardSize; i++)
        {
            for (int j = 0; j < boardSize; j++)
            {
                cells[i, j].ChangeCondition(CellCondition.Blank);
            }
        }
    }

    public void ChangeCellCondition(int i, int j, CellCondition condition)
    {
        cells[i, j].ChangeCondition(condition);
    }

    public Cell GetCell(int i, int j)
    {
        return cells[i, j];
    }

    public void ResetBoard()
    {
        FillGameBoard();
    }

    public Cell[] GetEmptyCells()
    {
        List<Cell> freeCells = new List<Cell>();

        for (int i = 0; i < boardSize; i++)
        {
            for (int j = 0; j < boardSize; j++)
            {
                Cell cell = GetCell(i, j);
                if (cell.GetCondition() == CellCondition.Blank)
                {
                    freeCells.Add(cell);
                }
            }
        }

        return freeCells.ToArray();
    }

}
