using System.Text;
using UnityEngine;

public class BotPlayer : Player
{
    private static int botPlayerIndex = 0;

    public delegate void OnMadeMoveHandler(Cell cell);
    public event OnMadeMoveHandler OnMadeMove;

    public delegate void OnPlayerLoseHandler();
    public event OnPlayerLoseHandler OnPlayerLose;

    private Cell chosenCell;

    public BotPlayer(CellCondition side, GameBoard gameBoard, Game game) : base(side, gameBoard, game)
    {
        game.OnGameFinished += OnGameFinished;
        SetName();
    }

    public override void MakeMove()
    {
        Cell[] emptyCells = gameBoard.GetEmptyCells();
        int random = Random.Range(0, emptyCells.Length - 1);
        chosenCell = emptyCells[random];

        OnMadeMove?.Invoke(chosenCell);
    }

    public void ConfirmMove()
    {
        chosenCell.ChangeCondition(SideCondition);
        game.ConfirmMove();
    }

    private void OnGameFinished(Player winner)
    {
        if(winner != null && !winner.Equals(this))
        {
            OnPlayerLose?.Invoke();
            SetName();
        }
    }

    private void SetName()
    {
        botPlayerIndex++;
        StringBuilder _name = new StringBuilder("Bot_");
        _name.Append(botPlayerIndex);

        name = _name.ToString();
    }
}
