using System.Collections.Generic;

public class GameLogic
{

    private CellCondition lastTurnSide;

    private Dictionary<CellCondition, Player> playersSides;

    private readonly GameBoard gameBoard;
    private int moveCount;

    public delegate void OnMoveCountOverHandler();
    public event OnMoveCountOverHandler OnMoveCountOver;


    //private int[]


    private int maxCount
    {
        get
        {
            return gameBoard.boardSize * gameBoard.boardSize;
        }
    }

    public GameLogic(Player player1, Player player2, GameBoard gameBoard)
    {
        this.gameBoard = gameBoard;

        playersSides = new Dictionary<CellCondition, Player>();

        playersSides.Add(player1.SideCondition, player1);
        playersSides.Add(player2.SideCondition, player2);

        moveCount = 0;
    }

    public void PlayerTurn()
    {
        moveCount++;
        if (moveCount > maxCount)
        {
            OnMoveCountOver?.Invoke();
            return;
        }

        switch (lastTurnSide)
        {
            case CellCondition.Cross:
                lastTurnSide = CellCondition.Nought;
                playersSides[CellCondition.Nought].MakeMove();

                break;
            case CellCondition.Nought:
                lastTurnSide = CellCondition.Cross;
                playersSides[CellCondition.Cross].MakeMove();

                break;
        }

    }

    public Player GetWinner()
    {
        bool isCrossesWin = CheckWinner(CellCondition.Cross);
        bool isNoughtsWin = CheckWinner(CellCondition.Nought);

        if (isCrossesWin && isNoughtsWin)
        {
            return null;
        }

        if(isCrossesWin)
        {
            return playersSides[CellCondition.Cross];
        }

        if(isNoughtsWin)
        {
            return playersSides[CellCondition.Nought];
        }

        return null;
    }

    private bool CheckWinner(CellCondition condition)
    {
        // Rows
        return (gameBoard.GetCell(0, 0).GetCondition() == condition &&
             gameBoard.GetCell(0, 1).GetCondition() == condition &&
             gameBoard.GetCell(0, 2).GetCondition() == condition) ||

         (gameBoard.GetCell(1, 0).GetCondition() == condition &&
             gameBoard.GetCell(1, 1).GetCondition() == condition &&
             gameBoard.GetCell(1, 2).GetCondition() == condition) ||

         (gameBoard.GetCell(2, 0).GetCondition() == condition &&
             gameBoard.GetCell(2, 1).GetCondition() == condition &&
             gameBoard.GetCell(2, 2).GetCondition() == condition) ||
         // Columns
         (gameBoard.GetCell(0, 0).GetCondition() == condition &&
             gameBoard.GetCell(1, 0).GetCondition() == condition &&
             gameBoard.GetCell(2, 0).GetCondition() == condition) ||

         (gameBoard.GetCell(0, 1).GetCondition() == condition &&
             gameBoard.GetCell(1, 1).GetCondition() == condition &&
             gameBoard.GetCell(2, 1).GetCondition() == condition) ||

         (gameBoard.GetCell(0, 2).GetCondition() == condition &&
             gameBoard.GetCell(1, 2).GetCondition() == condition &&
             gameBoard.GetCell(2, 2).GetCondition() == condition) ||
         // Diagonals
         (gameBoard.GetCell(0, 0).GetCondition() == condition &&
             gameBoard.GetCell(1, 1).GetCondition() == condition &&
             gameBoard.GetCell(2, 2).GetCondition() == condition) ||

         (gameBoard.GetCell(0, 2).GetCondition() == condition &&
             gameBoard.GetCell(1, 1).GetCondition() == condition &&
             gameBoard.GetCell(2, 0).GetCondition() == condition);
    }

    public void Restart()
    {
        moveCount = 0;
        lastTurnSide = CellCondition.Nought;
    }

    public void SwapSides()
    {
        Player player;
        playersSides.TryGetValue(CellCondition.Cross, out player);

        playersSides[CellCondition.Cross] = playersSides[CellCondition.Nought];
        playersSides[CellCondition.Nought] = player;

        playersSides[CellCondition.Nought].ChangeSide(CellCondition.Nought);
        playersSides[CellCondition.Cross].ChangeSide(CellCondition.Cross);
    }

    public void ResetGameLogic()
    {
        OnMoveCountOver = null;
    }
}
