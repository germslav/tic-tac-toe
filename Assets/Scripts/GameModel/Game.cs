
public class Game
{
    private Player player1;
    private Player player2;

    private GameLogic gameLogic;
    private GameBoard gameBoard;

    public bool IsGameOver { get; private set; }

    public delegate void OnGameFinishedHandler(Player winningPlayer);
    public event OnGameFinishedHandler OnGameFinished;

    public Game(GameBoard gameBoard)
    {
        this.gameBoard = gameBoard;
    }

    public void InitPlayers(Player player1, Player player2)
    {
        if(gameLogic != null)
        {
            gameLogic.OnMoveCountOver -= GameLogic_OnMoveCountOver;
            gameLogic.ResetGameLogic();
        }

        this.player1 = player1;
        this.player2 = player2;

        gameLogic = new GameLogic(player1, player2, gameBoard);
        gameLogic.OnMoveCountOver += GameLogic_OnMoveCountOver;
    }

    public void StartGame()
    {
        IsGameOver = false;
        gameLogic.Restart();
        gameBoard.ResetGameBoard();

        gameLogic.PlayerTurn();
    }


    public Player FinishGame()
    {

        Player winningPlayer = gameLogic.GetWinner();
        OnGameFinished?.Invoke(winningPlayer);



        if(winningPlayer == null)
        {
            gameLogic.SwapSides();
        }

        return winningPlayer;
    }

    public void ConfirmMove()
    {
        gameLogic.PlayerTurn();
    }

    private void GameLogic_OnMoveCountOver()
    {
        IsGameOver = true;
    }
}
