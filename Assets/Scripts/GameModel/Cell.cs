
public enum CellCondition
{
    Blank,
    Cross,
    Nought
}

public class Cell
{
    private CellCondition condition;

    public delegate void OnCellChangedHandler();
    public event OnCellChangedHandler OnCellChanged;

    public Cell()
    {
        condition = CellCondition.Blank;
    }

    public void ChangeCondition(CellCondition value)
    {
        condition = value;
        OnCellChanged?.Invoke();
    }

    public CellCondition GetCondition()
    {
        return condition;
    }
}
