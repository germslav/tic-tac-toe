
public abstract class Player
{
    public CellCondition SideCondition { get; protected set; }

    protected GameBoard gameBoard;
    protected Game game;

    protected string name;

    public Player(CellCondition side, GameBoard gameBoard, Game game)
    {
        this.gameBoard = gameBoard;
        SideCondition = side;
        this.game = game;
    }

    public Player(CellCondition side, GameBoard gameBoard, Game game, string name) : this(side, gameBoard, game)
    {
        this.name = name;
    }


    public abstract void MakeMove();

    public string GetName()
    {
        return name;
    }

    public void ChangeSide(CellCondition changingValue)
    {
        SideCondition = changingValue;
    }
}
