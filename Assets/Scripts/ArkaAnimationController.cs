using UnityEngine;

public class ArkaAnimationController : MonoBehaviour
{
    [SerializeField]
    private GameObject robotRagdoll;
    [SerializeField]
    private Transform robotModel;

    void Start()
    {
        robotRagdoll = Instantiate(robotRagdoll);
        robotRagdoll.SetActive(false);
    }

    private void OnRagdollSpawn()
    {
        robotRagdoll.transform.position = robotModel.transform.position;
        robotRagdoll.transform.rotation = robotModel.transform.rotation;
        robotRagdoll.SetActive(true);
    }

    private void OnAnimationRepeat()
    {
        robotRagdoll.SetActive(false);
    }
}
