using UnityEngine;

public class RobotRagdollController : MonoBehaviour
{

    private Rigidbody[] rigidbodies;
    private Transform[] spines;

    private Quaternion[] startSpinesQuaternion;


    private Vector3[] startSpinesPosition;

    private void Awake()
    {

        rigidbodies = gameObject.GetComponentsInChildren<Rigidbody>(true);
        spines = GetComponentsInChildren<Transform>(true);
        startSpinesQuaternion = new Quaternion[spines.Length];
        startSpinesPosition = new Vector3[spines.Length];

        int i = 0;
        foreach(Transform spine in spines)
        {
            startSpinesQuaternion[i] = spine.localRotation;
            startSpinesPosition[i] = spine.localPosition;
            i++;
        }

    }

    private void OnEnable()
    {
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = false;
        }
    }

    private void OnDisable()
    {
        foreach (Rigidbody rigidbody in rigidbodies)
        {

            rigidbody.isKinematic = true;

            rigidbody.ResetCenterOfMass();
        }

        int i = 0;
        foreach (Transform spine in spines)
        {
            spine.localRotation = startSpinesQuaternion[i];
            spine.localPosition = startSpinesPosition[i];
            i++;
        }

    }
}
