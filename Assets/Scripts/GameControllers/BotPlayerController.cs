using System.Collections;
using UnityEngine;

public class BotPlayerController : MonoBehaviour
{
    private BotPlayer botPlayer;
    private GameController gameController;
    private GameBoardController gameBoardController;

    [SerializeField]
    private Transform movingTarget;
    private Vector3 movingTargetStartPosition;

    [SerializeField]
    private float moveSpeed = 2;

    private void Awake()
    {
        movingTargetStartPosition = movingTarget.position;
    }

    public void Init(BotPlayer botPlayer, GameController gameController, GameBoardController boardController)
    {
        this.botPlayer = botPlayer;
        this.gameController = gameController;
        botPlayer.OnMadeMove += BotPlayer_OnMadeMove;
        botPlayer.OnPlayerLose += BotPlayer_OnPlayerLose;
        gameBoardController = boardController;

    }

    private void BotPlayer_OnPlayerLose()
    {
        //� ���� ����� ����� ������� �������� ��������� ����.
    }

    private void BotPlayer_OnMadeMove(Cell cell)
    {
        StartMoveAnimation(cell);
    }

    private void StartMoveAnimation(Cell cell)
    {
        Vector3 moveTarget = gameBoardController.GetCellPosition(cell);

        StartCoroutine(MoveToCellAnimation(moveTarget));
    }

    
    IEnumerator MoveToCellAnimation(Vector3 moveTarget)
    {
        float startTime = Time.time;
        Vector3 startPos = movingTarget.position;

        float journeyLength = Vector3.Distance(startPos, moveTarget);


        float distCovered = (Time.time - startTime) * moveSpeed * 0.5f;

        float fractionOfJourney = distCovered / journeyLength;

        while(fractionOfJourney < 1f)
        {
            distCovered = (Time.time - startTime) * moveSpeed;

            fractionOfJourney = distCovered / journeyLength;

            movingTarget.position = Vector3.Lerp(startPos, moveTarget, fractionOfJourney);

            yield return new WaitForSeconds(0.01f);
        }
        OnAnimationChangeCell();
    }

    IEnumerator MoveToStartPos(Vector3 moveTarget)
    {
        float startTime = Time.time;
        Vector3 startPos = movingTarget.position;
        float journeyLength = Vector3.Distance(startPos, moveTarget);
        float distCovered = (Time.time - startTime) * moveSpeed * 1f;

        float fractionOfJourney = distCovered / journeyLength;
        while (fractionOfJourney < 1f)
        {
            distCovered = (Time.time - startTime) * moveSpeed * 1f;
            fractionOfJourney = distCovered / journeyLength;
            movingTarget.position = Vector3.Lerp(startPos, moveTarget, fractionOfJourney);

            yield return new WaitForSeconds(0.01f);
        }
    }

    private void OnAnimationChangeCell()
    {
        StartCoroutine(MoveToStartPos(movingTargetStartPosition));
        botPlayer.ConfirmMove();
    }
}
