using UnityEngine;

public class CellController : MonoBehaviour
{
    [SerializeField]
    private GameObject cross;
    [SerializeField]
    private GameObject circle;

    private Cell cell;

    public void Init(Cell cell)
    {
        this.cell = cell;
        cell.OnCellChanged += Cell_OnCellChanged;
    }

    private void Cell_OnCellChanged()
    {
        CellCondition condition = cell.GetCondition();

        switch(condition)
        {
            case CellCondition.Blank:
                cross.SetActive(false);
                circle.SetActive(false);
                break;
            case CellCondition.Cross:
                cross.SetActive(true);
                break;
            case CellCondition.Nought:
                circle.SetActive(true);
                break;
        }
    }
}
