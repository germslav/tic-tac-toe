using System.Collections.Generic;
using UnityEngine;

public class GameBoardController : MonoBehaviour
{
    public Dictionary<Cell, CellController> Cells { get; private set; }

    private GameBoard gameBoard;

    public void Init(GameBoard gameBoard)
    {
        this.gameBoard = gameBoard;

        Cells = new Dictionary<Cell, CellController>();

        CellController[] cellControllers = transform.GetChild(0).GetComponentsInChildren<CellController>();

        for(int i = 0; i < gameBoard.boardSize; i++)
        {
            for(int j = 0; j < gameBoard.boardSize; j++)
            {
                CellController cellController = cellControllers[i * 3 + j];
                Cell cell = gameBoard.GetCell(i, j);
                cellController.Init(cell);

                Cells.Add(cell, cellController);
            }
        }
    }


    public Vector3 GetCellPosition(Cell cell)
    {
        return Cells[cell].transform.position;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
