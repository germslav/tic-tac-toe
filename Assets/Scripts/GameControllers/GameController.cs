using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private BotPlayerController botPlayerController1;
    [SerializeField]
    private BotPlayerController botPlayerController2;
    [SerializeField]
    private GameBoardController gameBoardController1;
    [SerializeField]
    private GameBoardController gameBoardController2;

    private Game game;
    private bool isAlreadyFinished;
    private bool isAlreadyStart;

    public delegate void OnHaveResultHandler(Player winner);
    public event OnHaveResultHandler OnHaveResult;

    public GameBoard GameBoard { get; private set; }

    private void Awake()
    {
        GameBoard = new GameBoard(3);
        gameBoardController1.Init(GameBoard);
        gameBoardController2.Init(GameBoard);
        
        game = new Game(GameBoard);

        BotPlayer player1 = new BotPlayer(CellCondition.Cross, GameBoard, game);
        botPlayerController1.Init(player1, this, gameBoardController1);
        BotPlayer player2 = new BotPlayer(CellCondition.Nought, GameBoard, game);
        botPlayerController2.Init(player2, this, gameBoardController2);

        game.InitPlayers(player1, player2);

    }

    public void StartGame()
    {
        if(isAlreadyStart) //�� ����� �� ������
        {
            return;
        }

        isAlreadyFinished = false;
        isAlreadyStart = true;
        game.StartGame();
    }

    public void ConfirmMove()
    {
        game.ConfirmMove();
    }

    public void EndGame()
    {
        if(isAlreadyFinished || !game.IsGameOver) //�� ����� �� ������
        {
            return;
        }

        isAlreadyFinished = true;
        isAlreadyStart = false;
        Player winingPlayer = game.FinishGame();
        OnHaveResult?.Invoke(winingPlayer);
    }
}
