using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    [SerializeField]
    private GameController gameController;

    [SerializeField]
    private Text resultText;
    [SerializeField]
    private GameObject resultBoard;

    private GameBoard gameBoard;

    private Animator animator;


    public void Start()
    {
        animator = GetComponent<Animator>();


        gameBoard = gameController.GameBoard;

        CellController[] cellControllers = transform.GetChild(0).GetComponentsInChildren<CellController>();

        for(int i = 0; i < gameBoard.boardSize; i++)
        {
            for(int j = 0; j < gameBoard.boardSize; j++)
            {
                CellController cellController = cellControllers[i * 3 + j];
                Cell cell = gameBoard.GetCell(i, j);
                cellController.Init(cell);
            }
        }

        gameController.OnHaveResult += GameController_OnHaveResult;
    }

    private void GameController_OnHaveResult(Player winner)
    {
        resultBoard.SetActive(true);
        animator.SetBool("isFinished", true);

        if(winner == null)
        {
            resultText.text = "Draw";
            return;
        }

        CellCondition winCondition = winner.SideCondition;

        switch(winCondition)
        {
            case CellCondition.Cross:
                resultText.text = "Crosses Win";
                break;
            case CellCondition.Nought:
                resultText.text = "Noughts Win";
                break;
        }
    }

    public void OnExaminateButtonPressed()
    {
        animator.SetBool("isFinished", false);
        resultBoard.SetActive(false);
        gameController.StartGame();
    }

    public void OnGetWinnerButtonPressed()
    {
        gameController.EndGame();
    }

}
