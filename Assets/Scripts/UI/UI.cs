using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField]
    private GameObject startRoomCamera;
    [SerializeField]
    private GameObject playRoomCamera;

    [SerializeField]
    private GameObject gameUI;
    [SerializeField]
    private GameObject startUI;


    public void OnGoGameRoomButtonPressed()
    {
        playRoomCamera.SetActive(true);
        startRoomCamera.SetActive(false);
        startUI.SetActive(false);
        gameUI.SetActive(true);
    }

    public void OnGoStartRoomButtonPressed()
    {
        startRoomCamera.SetActive(true);
        playRoomCamera.SetActive(false);
        gameUI.SetActive(false);
        startUI.SetActive(true);
    }

}
